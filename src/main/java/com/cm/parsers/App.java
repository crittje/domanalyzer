package com.cm.parsers;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

public class App {

    private static final Logger logger = LogManager.getLogger(App.class);
    private final static String INPUT_FILE = "config/input-list.cfg";
    private final static String PROPERTIES_FILE = "config/config.properties";
    private final static String OUTPUT_FILE = "output/results.txt";
    private static FileHandler fileHandler = new FileHandler();
    private static ArrayList<String> inputList;
    private static ArrayList<String> outputList;
    private static Properties properties;

    public static void main(String[] args) {
        logger.info("Starting app");
        App app = new App();
        app.init();
        logger.info("App down");
    }

    private void init() {
        inputList = fileHandler.readFile(INPUT_FILE);
        outputList = fileHandler.readFile(OUTPUT_FILE);
        properties = fileHandler.readPropertiesFile(PROPERTIES_FILE);
        if (properties.size() == 0) {
            logger.warn("Shutting down app, as no properties are found");
            System.exit(-1);
        }
        final String pageToLoad = properties.getProperty("page");
        logger.info("Page to load: " + pageToLoad);
        startDownloadingDom(pageToLoad);

    }

    private void startDownloadingDom(String pageToLoad) {
       try (WebClient webClient = new WebClient()) {
            HtmlPage page = webClient.getPage(pageToLoad);
            DomNodeList<DomElement> nodesToCheck = page.getElementsByTagName(properties.getProperty("main-element"));
            for (DomElement domElement : nodesToCheck) {
                lookForGivenNodes(domElement, properties.getProperty("sub-element"));
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    private void lookForGivenNodes(DomElement domElement, String subElementToFind) {
        DomNodeList<HtmlElement> foundSubElements = domElement.getElementsByTagName(subElementToFind);
        if (!foundSubElements.isEmpty()) {
            for (DomElement element : foundSubElements) {
                String value = element.getTextContent().trim();
                if (isValueInteresting(value.toLowerCase())) {
                    storeRelevantInformation(element);
                }
            }
        }
    }

    private void storeRelevantInformation(DomElement element) {
        String streetName = element.getParentNode().getParentNode().getParentNode().getParentNode()
                .getHtmlElementDescendants().iterator().next().getElementsByTagName("h4").get(0).getTextContent().trim();
        if(!isAlreadyInList(streetName)) {
            logger.info("New relevant info found");
            DomNodeList<HtmlElement> paragraphs = (element.getParentNode().getParentNode().getParentNode().getParentNode()
                    .getHtmlElementDescendants().iterator().next().getElementsByTagName("p"));
            NewProperty newProperty = new NewProperty(streetName, paragraphs.get(0).getTextContent().trim(),
                    paragraphs.get(1).getTextContent().trim());
            fileHandler.appendToFile(streetName + ";" + newProperty.getPostal()+ ";" + newProperty.getPrice(), OUTPUT_FILE);
            try {
                MailHandler mailHandler = new MailHandler(newProperty, properties);
                mailHandler.createMail();
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
    }

    private boolean isAlreadyInList(String streetName) {
        for (String row : outputList) {
            String[] splitter = row.split(";");
            if (splitter[0].equals(streetName)) {
                return true;
            }
        }

        return false;
    }

    private boolean isValueInteresting(String value) {
        for (String input : inputList) {
            if (value.contains(input)) {
                return true;
            }
        }
        return false;
    }
}
