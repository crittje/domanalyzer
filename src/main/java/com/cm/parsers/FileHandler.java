package com.cm.parsers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Properties;

class FileHandler {

    private Logger logger = LogManager.getLogger("FileHandler");

    ArrayList<String> readFile(String fileName) {
        ArrayList<String> list = new ArrayList<>();
        try {
            BufferedReader br = new BufferedReader(new FileReader(fileName));
            String newLine;
            while ((newLine = br.readLine()) != null) {
                list.add(newLine);
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        logger.debug("File {} has size of {}", fileName, list.size());
        return list;
    }

    Properties readPropertiesFile(String fileName) {
        Properties properties = new Properties();

        try (InputStream input = new FileInputStream(fileName)) {
            properties.load(input);
            return properties;
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return null;
    }

    void appendToFile(String toAppend, String fileName) {
        BufferedWriter bw = null;
        File file = new File(fileName);
        if (!file.exists()) {
            return;
        }
        try {
            bw = new BufferedWriter(new FileWriter(file, true));
            bw.write(toAppend);
            bw.newLine();
        } catch (IOException e) {
            logger.error(e.getMessage());
        } finally {
            try {
                if (bw != null) {
                    bw.close();
                }
            } catch (IOException e) {
                logger.error(e.getMessage());
            }
        }
    }
}
