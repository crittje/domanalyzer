package com.cm.parsers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.Properties;

class MailHandler {

    private Logger logger = LogManager.getLogger(this.getClass());
    private NewProperty newProperty;
    private Properties properties;
    private Properties mailProps = new Properties();

    MailHandler(NewProperty newProperty, Properties properties) throws Exception {
        this.newProperty = newProperty;
        this.properties = properties;

        mailProps = new Properties();
        mailProps.put("mail.smtp.auth", "true");
        mailProps.put("mail.smtp.starttls.enable", "true");
        mailProps.put("mail.smtp.host", properties.getProperty("host"));
        mailProps.put("mail.smtp.port", properties.getProperty("port"));
    }

    void createMail() {
        logger.info("Creating mail");

        Session session = Session.getInstance(mailProps, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(properties.getProperty("from-address"), properties.getProperty("from-pass"));
            }
        });

        try {
            MimeMessage msg = new MimeMessage(session);
            msg.setFrom(properties.getProperty("from-address"));
            msg.setRecipients(Message.RecipientType.TO, properties.getProperty("to-address"));
            msg.setSubject("New property");
            msg.setSentDate(new Date());
            msg.setText("Price: " + newProperty.getPrice()+ "\nPostal: " + newProperty.getPostal() +
                    "\nStreet: " + newProperty.getStreetName());
            Transport.send(msg);
            logger.info("Mail send");
        } catch (MessagingException e) {
            logger.error(e.getMessage());
        }
    }

}
