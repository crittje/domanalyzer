package com.cm.parsers;

class NewProperty {

    private String streetName;
    private String postal;
    private String price;

    NewProperty(String streetName, String postal, String price) {
        this.setStreetName(streetName);
        this.setPostal(postal);
        this.setPrice(price);
    }

    String getStreetName() {
        return streetName;
    }

    private void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    String getPostal() {
        return postal;
    }

    private void setPostal(String postal) {
        this.postal = postal;
    }

    String getPrice() {
        return price;
    }

    private void setPrice(String price) {
        this.price = price;
    }
}
